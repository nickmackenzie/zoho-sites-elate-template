const zohoSites = require('neutrino-preset-zoho-sites-template');

module.exports = {
    options: {
        root: __dirname
    },
    use: [
        zohoSites()
    ]
};
