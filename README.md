# Elate template for Zoho Sites 2018

Source:
https://gitlab.com/ringods/zoho-sites-elate-template

Converted from the Elate theme:
https://freehtml5.co/elate-free-html5-bootstrap-template/

SASS style sheet structuring based on default setup for Atomic Design:
https://github.com/steveosoule/atomic-design-sass-outline
http://atomicdesign.bradfrost.com
